package com.mhssn.recyclerviewdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.mhssn.recyclerviewdemo.model.Tweet

class TweetsAdapter(private val tweetsList: List<Tweet>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            TweetType.Tweet.ordinal -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.tweet_item, parent, false)
                TweetViewHolder(view)
            }
            TweetType.TweetWithImage.ordinal -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.tweet_with_image_item, parent, false)
                TweetWithImageViewHolder(view)
            }
            else -> {
                throw IllegalArgumentException("type not supported")
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is TweetViewHolder -> holder.bind(tweetsList[position])
            is TweetWithImageViewHolder -> holder.bind(tweetsList[position])
        }
    }

    override fun getItemCount(): Int {
        return tweetsList.size
    }

    override fun getItemViewType(position: Int): Int {
        return when(tweetsList[position].imageUrl){
            null -> TweetType.Tweet.ordinal
            else -> TweetType.TweetWithImage.ordinal
        }
    }

    inner class TweetViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val username: TextView = itemView.findViewById(R.id.username)
        val twitterAccount: TextView = itemView.findViewById(R.id.twitterAccount)
        val avatar: ImageView = itemView.findViewById(R.id.avatarImage)
        val tweetBody: TextView = itemView.findViewById(R.id.tweetBodyText)
        val commentCount: TextView = itemView.findViewById(R.id.commentCount)
        val retweetCount: TextView = itemView.findViewById(R.id.retweetCount)
        val likeCount: TextView = itemView.findViewById(R.id.likeCount)

        fun bind(tweet: Tweet) {
            username.text = tweet.username
            twitterAccount.text = tweet.twitterAccount
            tweetBody.text = tweet.tweetBody
            commentCount.text = tweet.commentCount.toString()
            retweetCount.text = tweet.retweetCount.toString()
            likeCount.text = tweet.likeCount.toString()
            Glide.with(itemView)
                .load(tweet.avatarUrl)
                .into(avatar)
        }
    }

    inner class TweetWithImageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val username: TextView = itemView.findViewById(R.id.username)
        val twitterAccount: TextView = itemView.findViewById(R.id.twitterAccount)
        val avatar: ImageView = itemView.findViewById(R.id.avatarImage)
        val tweetBody: TextView = itemView.findViewById(R.id.tweetBodyText)
        val commentCount: TextView = itemView.findViewById(R.id.commentCount)
        val retweetCount: TextView = itemView.findViewById(R.id.retweetCount)
        val likeCount: TextView = itemView.findViewById(R.id.likeCount)
        val tweetImage: ImageView = itemView.findViewById(R.id.tweetImage)

        fun bind(tweet: Tweet) {
            username.text = tweet.username
            twitterAccount.text = tweet.twitterAccount
            tweetBody.text = tweet.tweetBody
            commentCount.text = tweet.commentCount.toString()
            retweetCount.text = tweet.retweetCount.toString()
            likeCount.text = tweet.likeCount.toString()
            Glide.with(itemView)
                .load(tweet.avatarUrl)
                .centerCrop()
                .into(avatar)

            Glide.with(itemView)
                .load(tweet.imageUrl)
                .transform(RoundedCorners(40))
                .into(tweetImage)
        }
    }

    enum class TweetType {
        Tweet, TweetWithImage
    }
}