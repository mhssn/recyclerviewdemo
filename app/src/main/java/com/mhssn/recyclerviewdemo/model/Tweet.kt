package com.mhssn.recyclerviewdemo.model

data class Tweet(
    val username: String,
    val twitterAccount: String,
    val avatarUrl: String,
    val tweetBody: String,
    val commentCount: Int,
    val retweetCount: Int,
    val likeCount: Int,
    val imageUrl: String? = null
)